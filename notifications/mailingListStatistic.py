import json

from rest_framework import status
from rest_framework.response import Response
from rest_framework.renderers import JSONRenderer
from drf_yasg import openapi, utils
from rest_framework.decorators import api_view, renderer_classes

from notifications.models import (
    MailModel,
    MailingListModel,
)
from notifications.serializers import MailRetrieveSerializer, MailingListSerializer

mailingList_id = openapi.Parameter('mailing_list_id', openapi.IN_QUERY, description='ID интересующей рассылки',
                                   type=openapi.TYPE_INTEGER)


@utils.swagger_auto_schema(method='get', manual_parameters=[mailingList_id, ], operation_description='Информация по конкретной рассылке', responses={
    '200': MailRetrieveSerializer,
    '404': 'Not found'
})
@api_view(('GET',))
@renderer_classes((JSONRenderer,))
def get_mailingListDetailStatistic(request):
    mails = MailModel.objects.filter(mailingList=request.query_params.get('mailing_list_id'))
    if not mails:
        return Response(data={'message': 'Рассылки с таким идентификатором не найдено'}, status=status.HTTP_404_NOT_FOUND)
    data = []
    for mail in mails.values_list():
        dictionary = {
            'id': str(mail[0]),
            'created_at': str(mail[1]),
            'is_sent': str(mail[2]),
            'mailingList': str(mail[3]),
            'destination_client': str(mail[4])
        }
        data.append(dictionary)
    return Response(data=data, status=status.HTTP_200_OK)


@utils.swagger_auto_schema(method='get', operation_description='Информация по рассылкам и количеству писем в них')
@api_view(('GET',))
@renderer_classes((JSONRenderer,))
def get_mailingListGeneralStatistic(request):
    mailingLists = MailingListModel.objects.all()
    if not mailingLists.count():
        return Response(data={'message': 'В настоящий момент рассылок нет'}, status=status.HTTP_200_OK)
    data = []
    for mailing_list in mailingLists.values_list():
        count = MailModel.objects.filter(mailingList=mailing_list[0]).count()
        dictionary = {
            'id': str(mailing_list[0]),
            'started_at': str(mailing_list[1]),
            'text': mailing_list[2],
            'filter_mobile_code': mailing_list[3],
            'filter_tag': mailing_list[4],
            'stop_at': str(mailing_list[5]),
            'mails_count': str(count)
        }
        data.append(dictionary)
    return Response(data=data, status=status.HTTP_200_OK)
