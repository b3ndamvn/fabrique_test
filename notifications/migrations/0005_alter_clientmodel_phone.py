# Generated by Django 4.1 on 2022-08-25 23:35

import django.core.validators
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('notifications', '0004_alter_clientmodel_phone'),
    ]

    operations = [
        migrations.AlterField(
            model_name='clientmodel',
            name='phone',
            field=models.CharField(max_length=20, validators=[django.core.validators.RegexValidator(message='Неверный формат номера телефона', regex='^((7)+([0-9]){10})$')]),
        ),
    ]
