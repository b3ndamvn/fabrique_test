from django.contrib import admin
from notifications.models import (
    MailingListModel,
    MailModel,
    ClientModel,
)


admin.site.register(MailingListModel)
admin.site.register(MailModel)
admin.site.register(ClientModel)
