import datetime
import time

from django.db import models
from django.core.validators import RegexValidator
import pytz
from rest_framework.exceptions import ValidationError


class MailingListModel(models.Model):
    started_at = models.DateTimeField()
    text = models.TextField()
    filter_mobile_code = models.CharField(max_length=10)
    filter_tag = models.CharField(max_length=20)
    stop_at = models.DateTimeField()

    def save(self, *args, **kwargs):
        if self.started_at > self.stop_at:
            raise ValidationError("Время начала не может быть позже времени конца")
        if time.mktime(self.stop_at.timetuple()) < time.time():
            raise ValidationError("Время окончания рассылки не может быть меньше текущего времени")
        super(MailingListModel, self).save(*args, **kwargs)

    def __str__(self):
        return f'{str(self.started_at)} | {self.filter_tag}: {self.filter_mobile_code}'


class ClientModel(models.Model):
    TIMEZONES = tuple(zip(pytz.common_timezones, pytz.common_timezones))
    phone = models.CharField(max_length=20, unique=True, validators=[
        RegexValidator(
            regex='^((7)+([0-9]){10})$',
            message='Неверный формат номера телефона',
        ),
        ]
    )
    operator_code = models.CharField(max_length=10)
    tag = models.CharField(max_length=20)
    timezone = models.CharField(max_length=50, choices=TIMEZONES)

    def save(self, *args, **kwargs):
        self.operator_code = self.phone[1:4]
        super().save(*args, **kwargs)

    def __str__(self):
        return str(self.id)


class MailModel(models.Model):
    created_at = models.DateTimeField(auto_now=True, auto_created=True)
    is_sent = models.BooleanField(default=False)
    mailingList = models.ForeignKey(MailingListModel, on_delete=models.CASCADE, related_name='mailing_list')
    destinationClient = models.ForeignKey(ClientModel, on_delete=models.CASCADE, related_name='destination_client')

    def __str__(self):
        return f'{str(self.created_at)} ({str(self.is_sent)})'
