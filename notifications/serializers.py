from rest_framework import serializers
from rest_framework.serializers import ModelSerializer

from notifications.models import (
    MailingListModel,
    ClientModel,
    MailModel,
)


class MailingListSerializer(ModelSerializer):
    started_at = serializers.DateTimeField(format="%d-%m-%Y %H:%M:%S")
    stop_at = serializers.DateTimeField(format="%d-%m-%Y %H:%M:%S")
    class Meta:
        model = MailingListModel
        fields = '__all__'


class ClientCreateSerializer(ModelSerializer):
    class Meta:
        model = ClientModel
        fields = ('phone', 'tag', 'timezone')


class ClientRetrieveSerializer(ModelSerializer):
    class Meta:
        model = ClientModel
        fields = '__all__'


class MailCreateSerializer(ModelSerializer):
    class Meta:
        model = MailModel
        fields = ('id', 'mailingList', 'destinationClient')


class MailRetrieveSerializer(ModelSerializer):
    class Meta:
        model = MailModel
        fields = '__all__'
