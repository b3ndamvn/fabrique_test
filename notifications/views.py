from rest_framework.generics import (
    ListCreateAPIView,
)
from notifications.models import (
    MailingListModel,
    ClientModel,
    MailModel,
)
from notifications.serializers import (
    MailingListSerializer,
    ClientCreateSerializer,
    ClientRetrieveSerializer,
    MailCreateSerializer,
    MailRetrieveSerializer,
)
from rest_framework import viewsets


class MailingListCreateView(ListCreateAPIView):
    serializer_class = MailingListSerializer
    queryset = MailingListModel.objects.all()


class ClientViewSet(viewsets.ModelViewSet):
    queryset = ClientModel.objects.all()

    def get_serializer_class(self):
        if self.action == 'list':
            return ClientRetrieveSerializer

        return ClientCreateSerializer


class MailViewSet(viewsets.ModelViewSet):

    queryset = MailModel.objects.all()

    def get_serializer_class(self):
        if self.action == 'list':
            return MailRetrieveSerializer

        return MailCreateSerializer
