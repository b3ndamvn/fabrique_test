import datetime
import json

from django.test import TestCase

from rest_framework.test import (
    APITestCase,
    APIClient,
)
from rest_framework import status

from notifications.models import ClientModel, MailingListModel, MailModel


class MailingListTests(APITestCase):
    def test_mailing_list_list(self):
        client = APIClient()
        response = client.get('/mailing_list/')
        self.assertEquals(response.status_code, status.HTTP_200_OK)

    def test_mailing_list_create(self):
        client = APIClient()
        content = {
            "started_at": "2023-08-26 18:41:34",
            "stop_at": "2023-08-27 19:41:34",
            "text": "test",
            "filter_mobile_code": "123",
            "filter_tag": "test tag"
        }
        response = client.post('/mailing_list/', data=content)
        self.assertEquals(response.status_code, status.HTTP_201_CREATED)


class ClientTest(APITestCase):
    def setUp(self):
        self.client = ClientModel.objects.create(phone='79123456789', tag='testTag', timezone='Europe/Moscow')

        self.client_valid_data = {
            'phone': self.client.phone,
            'tag': self.client.tag,
            'timezone': self.client.timezone
        }
        self.client_invalid_data = {
            'phone': '89123456700',
            'tag': self.client.tag,
            'timezone': self.client.timezone
        }
        self.second_client_valid_data = {
            'phone': '79123450000',
            'tag': self.client.tag,
            'timezone': self.client.timezone
        }

    def test_client_list(self):
        client = APIClient()
        response = client.get('/clients/')
        self.assertEquals(response.status_code, status.HTTP_200_OK)

    def test_client_create(self):
        client = APIClient()
        response = client.post('/clients/', data=self.second_client_valid_data)
        self.assertEquals(response.status_code, status.HTTP_201_CREATED)
        response = client.post('/clients/', data=self.client_invalid_data)
        self.assertEquals(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_client_put(self):
        client = APIClient()
        response = client.put(f'/clients/{self.client.id}/', data={
            "phone": "79123456789",
            "tag": "tagTag",
            "timezone": "Europe/Moscow"
        })
        self.assertEquals(response.status_code, status.HTTP_200_OK)

    def test_client_patch(self):
        client = APIClient()
        content = {
            "tag": "tag tag"
        }
        response = client.patch(f'/clients/{self.client.id}/', data=content)
        self.assertEquals(response.status_code, status.HTTP_200_OK)

    def test_client_delete(self):
        client = APIClient()
        client.post('/clients/', data=self.client_valid_data)
        response = client.delete(f'/clients/{self.client.id}/')
        self.assertEquals(response.status_code, status.HTTP_204_NO_CONTENT)


class MailTests(APITestCase):
    def setUp(self):
        self.client = ClientModel.objects.create(phone='79123456789', tag='testTag', timezone='Europe/Moscow')
        self.second_client = ClientModel.objects.create(phone='79123450000', tag='testTag', timezone='Europe/Moscow')
        self.mailing_list = MailingListModel.objects.create(started_at=datetime.datetime(2023, 8, 26, 18, 41, 34, 227),
                                                            stop_at=datetime.datetime(2023, 8, 27, 18, 41, 34, 227),
                                                            text='test', filter_mobile_code='123',
                                                            filter_tag='test tag')
        self.mail = MailModel.objects.create(mailingList=self.mailing_list, destinationClient=self.client)
        self.mail_data = {
            'mailingList': self.mailing_list.id,
            'destinationClient': self.client.id
        }

        self.client_valid_data = {
            'phone': self.client.phone,
            'tag': self.client.tag,
            'timezone': self.client.timezone
        }
        self.client_invalid_data = {
            'phone': '89123456789',
            'tag': self.client.tag,
            'timezone': self.client.timezone
        }

        self.client_second_valid_data = {
            'phone': self.second_client.phone,
            'tag': self.second_client.tag,
            'timezone': self.second_client.timezone
        }

        self.mailing_list_data = {
            'started_at': self.mailing_list.started_at,
            'stop_at': self.mailing_list.stop_at,
            'text': self.mailing_list.text,
            'filter_mobile_code': self.mailing_list.filter_mobile_code,
            'filter_tag': self.mailing_list.filter_tag
        }

    def test_mail_list(self):
        client = APIClient()
        response = client.get('/mails/')
        self.assertEquals(response.status_code, status.HTTP_200_OK)

    def test_mail_create(self):
        client = APIClient()
        response = client.post('/mails/', data=self.mail_data)
        self.assertEquals(response.status_code, status.HTTP_201_CREATED)

    def test_mail_put(self):
        client = APIClient()
        response = client.put(f'/mails/{self.mail.id}/', data={
            'mailingList': self.mailing_list.id,
            'destinationClient': self.second_client.id
        })
        self.assertEquals(response.status_code, status.HTTP_200_OK)

    def test_mail_patch(self):
        client = APIClient()
        response = client.patch(f'/mails/{self.mail.id}/', data={
            "destinationClient": self.second_client.id
        })
        self.assertEquals(response.status_code, status.HTTP_200_OK)

    def test_mail_delete(self):
        client = APIClient()
        response = client.delete(f'/mails/{self.mail.id}/')
        self.assertEquals(response.status_code, status.HTTP_204_NO_CONTENT)


class MailingListDetailStatisticTest(APITestCase):
    def setUp(self):
        self.client = ClientModel.objects.create(phone='79123456789', tag='testTag', timezone='Europe/Moscow')
        self.mailing_list = MailingListModel.objects.create(started_at=datetime.datetime(2023, 8, 26, 18, 41, 34, 227),
                                                            stop_at=datetime.datetime(2023, 8, 27, 18, 41, 34, 227),
                                                            text='test', filter_mobile_code='123',
                                                            filter_tag='test tag')
        self.mail = MailModel.objects.create(mailingList=self.mailing_list, destinationClient=self.client)

    def test_mailing_list_detail_statistic_list(self):
        client = APIClient()
        response = client.get(f'/mailing_list_detail_statistic/?mailing_list_id={self.mailing_list.id}')
        self.assertEquals(response.status_code, status.HTTP_200_OK)


class MailingListGeneralStatisticTest(APITestCase):
    def setUp(self):
        self.client = ClientModel.objects.create(phone='79123456789', tag='testTag', timezone='Europe/Moscow')
        self.mailing_list = MailingListModel.objects.create(started_at=datetime.datetime(2023, 8, 26, 18, 41, 34, 227),
                                                            stop_at=datetime.datetime(2023, 8, 27, 18, 41, 34, 227),
                                                            text='test', filter_mobile_code='123',
                                                            filter_tag='test tag')
        self.mail = MailModel.objects.create(mailingList=self.mailing_list, destinationClient=self.client)
        self.mail_data = {
            'mailingList': self.mailing_list.id,
            'destinationClient': self.client.id
        }

        self.client_valid_data = {
            'phone': self.client.phone,
            'tag': self.client.tag,
            'timezone': self.client.timezone
        }

        self.mailing_list_data = {
            'started_at': self.mailing_list.started_at,
            'stop_at': self.mailing_list.stop_at,
            'text': self.mailing_list.text,
            'filter_mobile_code': self.mailing_list.filter_mobile_code,
            'filter_tag': self.mailing_list.filter_tag
        }

    def test_mailing_list_general_statistic_list(self):
        client = APIClient()
        client.post('/clients/', data=self.client_valid_data)
        client.post('/mailing_list/', data=self.mailing_list_data)
        client.post('/mails/', data=self.mail_data)
        response = client.get('/mailing_list_general_statistic/')
        self.assertEquals(response.status_code, status.HTTP_200_OK)
