FROM python:3
ENV PYTHONUNBUFFERED=1

WORKDIR /fabriqueTest

COPY requirements.txt /fabriqueTest/

RUN pip install --no-cache-dir --upgrade -r requirements.txt

COPY . /fabriqueTest/