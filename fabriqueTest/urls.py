from django.contrib import admin
from django.urls import path
from drf_yasg.views import get_schema_view
from drf_yasg import openapi
from rest_framework import permissions
from rest_framework.routers import SimpleRouter

from notifications.views import *

from notifications.mailingListStatistic import (
    get_mailingListDetailStatistic,
    get_mailingListGeneralStatistic,
)


schema_view = get_schema_view(
   openapi.Info(
      title="Fabrique API",
      default_version='v1',
      description="Тестовое задание",
   ),
   public=True,
   permission_classes=(permissions.AllowAny,),
)

router = SimpleRouter()
router.register(r'clients', ClientViewSet)
router.register(r'mails', MailViewSet)

urlpatterns = [
    path('docs/', schema_view.with_ui(cache_timeout=0)),
    path('admin/', admin.site.urls),

    path('mailing_list/', MailingListCreateView.as_view()),

    path('mailing_list_detail_statistic/', get_mailingListDetailStatistic),
    path('mailing_list_general_statistic/', get_mailingListGeneralStatistic),
]

urlpatterns += router.urls
